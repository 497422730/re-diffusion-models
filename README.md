.1 /.
.2 improved-diffusion -- files for diffusion models.
.3 setup.py -- set up diffusion models.
.3 ....
.2 improved-diffusion-v7-7 -- files for inpainting diffusion models.
.3 setup.py -- set up inpainting diffusion models.
.3 ....
.2 KAIR -- files for denoising tasks.
.3 ....
.2 moco -- files for moco.
.3 ....
.2 SimCLR -- files for SimCLR.
.3 ....
.2 v7-3c.ipynb -- classification task for w/o pretrained model with input and middle block.
.2 v7-3c-back.ipynb -- classification task for w/o pretrained model with input and middle block.
.2 v7-3c-mid.ipynb -- classification task for w/o pretrained model with input, middle and output block.
.2 v7-3c-50.ipynb -- denoising task for w/o pretrained model with noise level 50.
.2 v7-3c-100.ipynb -- denoising task for w/o pretrained model with noise level 100.
.2 v7-4c.ipynb -- classification task for w/ pretrained model.
.2 v7-4c-pretrain.ipynb -- pretrain classification task by supervised learning.
.2 v7-5.ipynb -- pretrain model by backbone of diffusion models.
.2 v7-5(i).ipynb -- pretrain model by backbone of diffusion models with linear schedule and without learning sigma.
.2 v7-5(ii).ipynb -- pretrain model by backbone of diffusion models with cosine schedule and without learning sigma.
.2 v7-5(iii).ipynb -- pretrain model by backbone of diffusion models with linear schedule and with learning sigma.
.2 v7-5c.ipynb -- classification task for diffusion models.
.2 v7-5(i)c.ipynb -- classification task for diffusion models with linear schedule and without learning sigma.
.2 v7-5(ii)c.ipynb -- classification task for diffusion models with cosine schedule and without learning sigma.
.2 v7-5(iii)c.ipynb -- classification task for diffusion models with linear schedule and with learning sigma.
.2 v7-5c-49t.ipynb -- classification task for diffusion models with  $\mathbf{t}$ euqals 49.
.2 v7-5c-99t.ipynb -- classification task for diffusion models with  $\mathbf{t}$ euqals 99.
.2 v7-5c-149t.ipynb -- classification task for diffusion models with  $\mathbf{t}$ euqals 149.
.2 v7-5c-199t.ipynb -- classification task for diffusion models with  $\mathbf{t}$ euqals 199.
.2 v7-5c-randomt.ipynb -- classification task for diffusion models with random  $\mathbf{t}$.
.2 v7-5c-back.ipynb -- classification task fordiffusion models with input and middle block.
.2 v7-5c-mid.ipynb -- classification task for diffusion models with input, middle and output block.
.2 v7-5c-50.ipynb -- denoising task for diffusion models with noise level 50.
.2 v7-5c-100.ipynb -- denoising task for diffusion models with noise level 100.
.2 v7-5.ipynb -- pretrain model by backbone of inpainting diffusion models.
.2 v7-5c.ipynb -- classification task for inpainting diffusion models.
.2 v7-7c-50.ipynb -- denoising task for inpainting diffusion models with noise level 50.
.2 v7-7c-100.ipynb -- denoising task for inpainting diffusion models with noise level 100.
.2 v8-3.ipynb --  classification task for w/o pretrained model in the same dataset.
.2 v8-5.ipynb --  classification task for w/ pretrained model in the same dataset.
.2 v9-0.ipynb --  training of MoCo.
.2 v9-0c.ipynb --  classification task for MoCo.
.2 v9-1c.ipynb --  classification task for w/ pretrained model.
.2 v9-2c-pretrain.ipynb --  pretrain model by backbone of moco.
.2 v9-2c.ipynb --  classification task for w/o pretrained model.
.2 v9-3c.ipynb --  classification task for w/o pretrained model in the same dataset.
.2 v9-5.ipynb --  training of SimCLR in the same dataset.
.2 v9-5c.ipynb --  classification task for SimCLR in the same dataset.
.2 v10-0.ipynb --  training of SimCLR.
.2 v10-0c.ipynb --  classification task for SimCLR.
.2 v10-1c.ipynb --  classification task for w/ pretrained model.
.2 v10-2c-pretrain.ipynb --  pretrain model by backbone of SimCLR.
.2 v10-2c.ipynb --  classification task for w/o pretrained model.
.2 v10-3c.ipynb --  classification task for w/o pretrained model in the same dataset.
.2 v10-5.ipynb --  training of SimCLR in the same dataset.
.2 v10-5c.ipynb --  classification task for SimCLR in the same dataset.
.2 v11-0.ipynb --  training of context encoders.
.2 v11-0c.ipynb --  classification task for context encoders.
.2 v11-1c.ipynb --  classification task for w/ pretrained model.
.2 v11-2c-pretrain.ipynb --  pretrain model by backbone of context encoders.
.2 v11-2c.ipynb --  classification task for w/o pretrained model.
.2 v11-3c.ipynb --  classification task for w/o pretrained model in the same dataset.
.2 v11-5.ipynb --  training of context encoders in the same dataset.
.2 v11-5c.ipynb --  classification task for context encoders in the same dataset.
.2 msc-environment.ipynb Install necessary packages for the project..
.2 README.txt -- contains instructions on using the code.
